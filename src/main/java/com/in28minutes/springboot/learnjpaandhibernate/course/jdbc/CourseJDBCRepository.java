package com.in28minutes.springboot.learnjpaandhibernate.course.jdbc;

import com.in28minutes.springboot.learnjpaandhibernate.course.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CourseJDBCRepository {
    @Autowired
    private JdbcTemplate springJdbcTemplate;

    private static String INSERT_QUERY = """
            INSERT INTO course(id, name, author) VALUES(?, ?, ?);""";
    private static String DELETE_QUERY = """
            DELETE FROM course WHERE id = ?;""";

    private static String SELECT_QUERY = """
            SELECT * FROM course WHERE id = ?;""";

    public void insert(Course course) {
        springJdbcTemplate.update(INSERT_QUERY, course.getId(), course.getName(), course.getAuthor());
    }

    public void deleteById(long id) {
        springJdbcTemplate.update(DELETE_QUERY, id);
    }

    /*
      Map ResultSet to Bean by using Row Mapper.
      Course's Bean methods name are exactly similar to table's column names,
      so you can use BeanPropertyRowMapper<>() to map the resultSet to Course class.
    */
    public Course findById(long id) {
        return springJdbcTemplate.queryForObject(SELECT_QUERY,
                new BeanPropertyRowMapper<>(Course.class), id); // 3rd parameter id will be replace the ? in SELECT_QUERY.
    }
}
